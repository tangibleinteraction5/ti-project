/*
   Description
   -----------
   This program allows evaluation of the electromagnet setup. It loops through different power values that can be set in evaldata.h.

   The circuit:
 *    * Pushbutton connected to GND and pin 2 (no pullup needed, since the internal one is used)
 *    * Pins 8, 9 and 10 are connected to the respective transistors for the magnets.
*/

#include <Bounce2.h>
#include "evaldata.h"


const int pinMagnet1 = 3;   // connected to the base of the transistor that switches magnet 1
const int pinMagnet2 = 5;   // connected to the base of the transistor that switches magnet 2
const int pinMagnet3 = 6;   // connected to the base of the transistor that switches magnet 3

const int pinButton = 2;     // connected to the push button to switch round

// Instantiate a Bounce object
Bounce debouncer = Bounce();

int n_rounds = 16;     // The total number of rounds (should be align with evaldata.data)
int current_round;    // The current round number
int dataset[3];       // Holds the magnet power values for the current round in percent
int magnet_power[3];  // Holds the actual PWM values for setting the magnet power

void setup() {
  // Set the transistor pins as output
  pinMode(pinMagnet1, OUTPUT);
  pinMode(pinMagnet2, OUTPUT);
  pinMode(pinMagnet3, OUTPUT);

  // Setup the button with an internal pull-up
  pinMode(pinButton, INPUT_PULLUP);

  // Setup the Bounce instance
  debouncer.attach(pinButton);
  debouncer.interval(5); // interval in ms

  current_round = 0;

  Serial.begin(9600);

  Serial.println("Ready to begin Evaluation. Push button to start.");
}

void loop() {

  
  // Check if the button is pushed to update the round
  // Update the Bounce instance
  debouncer.update();

  // Check for low-to-high transition
  if (debouncer.rose()) {
    // Switch to next round
    updateRound();
  }

  /*** CHECK FOR OUTPUT TOGGLE ***/
  
  if (Serial.available()) {
    char c = Serial.read();

    // Switch to next round on Enter press (sends carriage return)
    if (c == '\r') {
      updateRound();
    }
  }

  /*** / CHECK FOR OUTPUT TOGGLE ***/



  if (current_round > 0) {

    // Set the magnet power according to these values
    setMagnets(magnet_power);

  }

}


/**
   Updates the round to the next one and prepares the magnet power data
*/
void updateRound() {

  current_round++;

  if (current_round <= n_rounds) {

    // Get the power values for the current round
    for (int i = 0; i < 2; i++) {
      dataset[i] = data[current_round - 1][i];
      // Convert to PWM output values
      magnet_power[i] = map(dataset[i], 0, 100, 0, 255);
    }

    Serial.print("Current round: ");
    Serial.println(current_round);

    Serial.print("Magnet Power: ");
    for (int i = 0; i < 2; i++) {
      Serial.print("    ");
      Serial.print(dataset[i]);
      Serial.print("%");
    }
    Serial.println();

  } else {
    // Stop evaluation loop
    current_round = 0;

    // Switch magnets off
    setMagnets(MAGNETS_OFF);
    
    Serial.println("Evaluation finished.");
    Serial.println();
  }
}

/**
   Sets the power of the magnets.
*/
void setMagnets (int magnet_power[]) {
  analogWrite(pinMagnet1, magnet_power[0]);
  //analogWrite(pinMagnet2, magnet_power[1]);
  analogWrite(pinMagnet3, magnet_power[1]);
}

