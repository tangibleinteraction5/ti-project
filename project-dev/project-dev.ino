
#include <EnableInterrupt.h>
//#include <Rotary.h>
#include <RotaryEncoder.h>

bool DEBUG = false;
const bool VERBOSE = false;

const bool PULL_MODE = true;    // Enables/disables a short, strong pull every time the magnets are switched on to reduce the gap between them and the objects.

const int PROD_WATER = 0;
const int PROD_APPLE = 1;
const int PROD_TEXTILE = 2;

const int VAL_WATER_USE = 0;
const int VAL_GREENHOUSE = 1;
const int VAL_TRSP_DIST = 2;

const String PRODUCT_NAMES[3] = {
  "Water", "Apples", "Textiles"
};

const String DISP_VALUE_NAMES[3] = {
  "Water Use", "Greenhouse Emissions", "Transport Distance"
};


const int pinMagnet1 = 3;   // connected to the base of the transistor that switches magnet 1
const int pinMagnet2 = 5;   // connected to the base of the transistor that switches magnet 2
const int pinMagnet3 = 6;   // connected to the base of the transistor that switches magnet 3

const int pinSwitch = 10;   // connected to the on/off switch

// Rotary Encoders
const int pinRotaryInnerA = 11;
const int pinRotaryInnerB = 12;
const int pinRotaryOuterA = 8;
const int pinRotaryOuterB = 9;


//Rotary rotInner = Rotary(pinRotaryInnerA, pinRotaryInnerB);     // Inner ring rotary encoder object
//Rotary rotOuter = Rotary(pinRotaryOuterA, pinRotaryOuterB);   // Outer ring rotary encoder object
RotaryEncoder encoderInner(pinRotaryInnerA, pinRotaryInnerB);
RotaryEncoder encoderOuter(pinRotaryOuterA, pinRotaryOuterB);

const float maxDeg = 360;                 // Rotary encoders constraints
const float innerDegPerStep = 0.58;          // Rotary encoders step size
const float outerDegPerStep = 0.72;//0.72;

// Inner Ring
//float innerRingSteps[3] = {0.0, 120.0, 240.0};   // Inner ring angle steps
//float innerRingPrecision = 60.0;         // How close to the position it should still be valid, in deg

float innerRingRanges[3][2] = {           // Outer ring angle ranges
  {  0.0, 120.0},
  {120.0, 240.0},
  {240.0, 360.0}
};

float innerRingDeg = 60.0;                   // Inner ring current position

// Outer ring
float outerRingRanges[3][2] = {           // Outer ring angle ranges
  {  0.0, 120.0},
  {120.0, 240.0},
  {240.0, 360.0}
};
float outerRingDeg = 60;                  // Outer ring current position: init at the middle of Water Use


int currentProductCat = 0;      // The currently selected product category
int currentDisplayVal = 0;      // The currently selected value to display

bool switchedOn = false;        // The state of the on/off switch
bool magnetsOn = false;         // The on/off state of the magnets (e.g., it should be false when between products)

// Variables for pulling mode
bool pulling = false;
unsigned long pullStartTime = 0;


unsigned long lastTime = 0;



int forces[3][3][3] = {         // Magnet power values (in %)
  // VALUE: WATER USE
  {
    {20, 0, 20},  // WATER       Domestic/tap/imported
    {70, 30, 70},  // APPLES     Organic/domestic/imported
    {50, 30, 100}   // TEXTILES  wool/polyester/cotton
  },
  // VALUE: GREENHOUSE GAS
  {
    {20,  0,  50},  // WATER
    {20,  0,  20},  // APPLES
    {40, 80,  60}   // TEXTILES
  },
  // VALUE: TRANSPORT DISTANCE
  {
    { 20,  0,  60},  // WATER
    { 60, 20, 60},  // APPLES
    {100, 20, 75}   // TEXTILES
  }
};

int maxForces[3] = {          // Maximum magnet power (0..255)
  255, 255, 255
};

float powerFactors[3] = {
  1.0, 1.5, 1.0
};


void setup() {


  // Set the transistor pins as output
  pinMode(pinMagnet1, OUTPUT);
  pinMode(pinMagnet2, OUTPUT);
  pinMode(pinMagnet3, OUTPUT);

  // Set the switch pin to use the internal pull-up resistor
  pinMode(pinSwitch, INPUT_PULLUP);

  // Set the interrupts for the rotary encoders
  enableInterrupt(pinRotaryInnerA, pollRotaryInner, CHANGE);
  enableInterrupt(pinRotaryInnerB, pollRotaryInner, CHANGE);
  enableInterrupt(pinRotaryOuterA, pollRotaryOuter, CHANGE);
  enableInterrupt(pinRotaryOuterB, pollRotaryOuter, CHANGE);

  encoderInner.setPosition(60 / innerDegPerStep);
  encoderOuter.setPosition(60 / outerDegPerStep);

  // Start serial output
  Serial.begin(9600);
  Serial.println("hello world.");

  Serial.print("Initializing at: ");
  Serial.print(PRODUCT_NAMES[currentProductCat]);
  Serial.print(", ");
  Serial.println(DISP_VALUE_NAMES[currentDisplayVal]);
  Serial.println();

  Serial.println("Switch on the Debug output by pressing Enter.");
  Serial.println("Set the line ending to 'Carriage return' first!");

  currentProductCat = 0; // TODO remove
}

void loop() {

  /*** CHECK FOR OUTPUT TOGGLE ***/

  if (Serial.available()) {
    char c = Serial.read();

    // Toggle on Enter press (sends carriage return)
    if (c == '\r') {
      DEBUG = !DEBUG;
      Serial.print("Switched debug output ");
      Serial.println(DEBUG ? "on" : "off");
    }
  }

  /*** / CHECK FOR OUTPUT TOGGLE ***/


  /*** GET ROTARY POSITIONS ***/

  int newInnerPos = limitEncoderRange(encoderInner, (360 / innerDegPerStep));
  int newOuterPos = limitEncoderRange(encoderOuter, (360 / outerDegPerStep));

  innerRingDeg = (float) newInnerPos * innerDegPerStep;
  outerRingDeg = (float) newOuterPos * outerDegPerStep;

  if (DEBUG && VERBOSE)
    printPosition(innerRingDeg);

  /*** / GET ROTARY POSITIONS ***/



  /*** DETERMINE MAGNET AND INNER RING STATE ***/


  // Check if magnets should be switched on,
  // depending on the switch state and the current rotational position of the inner ring.
  magnetsOn = false;

  // Read the value of the switch first
  bool newSwitchState = digitalRead(pinSwitch) == 0;
  if (newSwitchState != switchedOn) {
    switchedOn = newSwitchState;

    if (switchedOn && PULL_MODE) {
      pulling = true;
      pullStartTime = millis();
    }

    Serial.print("Switched power ");
    Serial.println(switchedOn ? "on" : "off");

  }

  magnetsOn = switchedOn;

  //if (switchedOn) {

  // Check the inner (magnet) ring positions
  for (int i = 0; i < 3; i++) {
    if (innerRingDeg >= innerRingRanges[i][0] &&
        innerRingDeg <  innerRingRanges[i][1]) {

      if (DEBUG) {
        if (i != currentProductCat) {
          Serial.print("Updated product category. New value: ");
          Serial.println(PRODUCT_NAMES[i]);
        }
      }

      // Set the currently selected value to display
      currentProductCat = i;
    }
  }
  //}

  /*** / DETERMINE MAGNET AND INNER RING STATE ***/



  if (DEBUG && VERBOSE) {
    //Serial.print("Magnet state: ");
    //Serial.println(magnetsOn);
  }



  /*** DETERMINE OUTER RING STATE ***/

  // Check the outer (display value) ring positions
  for (int i = 0; i < 3; i++) {
    if (outerRingDeg >= outerRingRanges[i][0] &&
        outerRingDeg <  outerRingRanges[i][1]) {

      if (DEBUG) {
        if (i != currentDisplayVal) {
          Serial.print("Updated display value. New value: ");
          Serial.println(DISP_VALUE_NAMES[i]);
        }
      }

      // Set the currently selected value to display
      currentDisplayVal = i;
    }
  }

  /*** / DETERMINE OUTER RING STATE ***/



  /*** UPDATE THE MAGNETS ***/

  setMagnets(magnetsOn, currentDisplayVal, currentProductCat);
  handlePulling();

  /*** / UPDATE THE MAGNETS ***/

  

}

/**
   Sets the power of the magnets to the values of the current category.
*/
void setMagnets (bool on, int value, int cat) {

  if (on) {
    analogWrite(pinMagnet1, min(map(forces[value][cat][0] * powerFactors[0], 0, 100, 0, maxForces[0]), 255));
    analogWrite(pinMagnet2, min(map(forces[value][cat][1] * powerFactors[1], 0, 100, 0, maxForces[1]), 255));
    analogWrite(pinMagnet3, min(map(forces[value][cat][2] * powerFactors[2], 0, 100, 0, maxForces[2]), 255));

    if (DEBUG && VERBOSE) {
      Serial.print("Power: ");
      for (int i = 0; i < 3; i++) {
        Serial.print("    ");
        Serial.print(forces[value][cat][i]);
      }
      Serial.println();

    }

  } else {
    // Write zeroes to the magnets to switch them off
    analogWrite(pinMagnet1, 0);
    analogWrite(pinMagnet2, 0);
    analogWrite(pinMagnet3, 0);
  }

}

void handlePulling() {

  if (pulling) {
    if (millis() - pullStartTime >= 500) {
      pulling = false;
    } else {
      analogWrite(pinMagnet1, 255);
      analogWrite(pinMagnet2, 255);
      analogWrite(pinMagnet3, 255);
    }
  }
}


/**
   Reads the rotation of the inner wheel and returns the position in degrees in [0, 360[
*/
/*
  float readRotation(Rotary &rotary, float currentDeg, float degPerStep) {

  //unsigned char result = rotary.process();

  if (result) {
    // If we get a result there was an update

    // Update current rot. position clockwise (+) or counterclockwise (-)

    if (result == DIR_CW) {
      currentDeg += degPerStep;
    } else {
      currentDeg -= degPerStep;
    }


    // Keep values in [0, 360[

    if (currentDeg >= maxDeg) {
      currentDeg -= maxDeg;
    }
    if (currentDeg < 0) {
      currentDeg += maxDeg;
    }

    if (DEBUG) {
      Serial.print("Rotary encoder position: ");
      Serial.println(currentDeg, 4);
    }

  } else {
    //Serial.println("no RE update.");
  }

  return currentDeg;

  }
*/

void pollRotaryInner() {
  //innerRingDeg = readRotation(rotInner, innerRingDeg, innerDegPerStep);
  encoderInner.tick();
}


void pollRotaryOuter() {
  //outerRingDeg = readRotation(rotOuter, outerRingDeg, outerDegPerStep);
  encoderOuter.tick();
}

int limitEncoderRange(RotaryEncoder &encoder, int maxValue) {
  int pos = encoder.getPosition();
  if (pos >= maxValue) {
    pos -= maxValue;
    encoder.setPosition(pos);
  }

  if (pos < 0) {
    pos += maxValue;
    encoder.setPosition(pos);
  }

  return pos;
}

void printPosition(float value) {
  unsigned long t = millis();
  if ( (t - lastTime) > 500 ) {
    lastTime = t;
    Serial.print("Encoder Position: ");
    Serial.println(value);
  }
}

